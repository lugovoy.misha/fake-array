import MyArray from '../index';

describe('tests for method pop', () => {
  test('instance has method pop', () => {
    const arr = new MyArray(1, 4, 0);

    expect(arr.pop).toBeInstanceOf(Function);
  });

  test('should delete the last item', () => {
    const a = new MyArray(1, 2, 3, 4);
    a.pop();
    expect(a).toEqual(new MyArray(1, 2, 3));
  });

  test('should  decrease length', () => {
    const a = new MyArray(1, 2, 3, 4);
    a.pop();
    expect(a).toHaveLength(3);
  });

  test('should return undefined if Array is empty', () => {
    const a = new MyArray();
    expect(a.pop()).toBeUndefined();
  });

  test('should return the deleted item', () => {
    const a = new MyArray(1, 2, 3);

    expect(a.pop()).toEqual(3);
  });

  test('should not take required arguments', () => {
    expect(MyArray.prototype.pop).toHaveLength(0);
  });
});
