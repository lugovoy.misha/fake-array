import MyArray from '../index';

describe('tests for instance', () => {
  test('instance does not have any own property except length', () => {
    const arr = new MyArray();
    expect(arr).toHaveLength(0);
    expect(Object.prototype.hasOwnProperty.call(arr, 'length')).toBeTruthy();
  });

  test('should throw an error if set length not a positive integer and bigger then 2^32', () => {
    const arr = new MyArray();
    expect(() => {
      arr.length = NaN;
    }).toThrowError('Invalid array length');
    expect(() => {
      arr.length = Infinity;
    }).toThrowError('Invalid array length');
    expect(() => {
      arr.length = 'string';
    }).toThrowError('Invalid array length');
    expect(() => {
      arr.length = {};
    }).toThrowError('Invalid array length');
    expect(() => {
      arr.length = Math.pow(2, 32);
    }).toThrowError('Invalid array length');
    expect(() => {
      arr.length = [3, 3];
    }).toThrowError('Invalid array length');
  });

  test('should set length converted to positive int values ', () => {
    const arr = new MyArray();
    arr.length = [3];
    expect(arr).toHaveLength(3);
    arr.length = '42';
    expect(arr).toHaveLength(42);
  });

  test('adding new elements should increase length of instance', () => {
    const arr = new MyArray();
    arr[5] = 'a';
    expect(arr).toHaveLength(6);
  });

  test('prototype have only declared methods and constructor', () => {
    const arr = new MyArray();
    const obj = {
      pop: {},
      push: {},
      shift: {},
      unshift: {},
      filter: {},
      map: {},
      forEach: {},
      reduce: {},
      toString: {},
      sort: {},
      constructor: {}
    };
    expect(Object.getPrototypeOf(arr)).toMatchObject(obj);
  });
});
