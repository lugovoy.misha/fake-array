import MyArray from '../index';

describe('tests for method push', () => {
  test('should work with NaN, null, undefined', () => {
    const arr = new MyArray(1, 2);
    arr.push(NaN);
    arr.push(null);
    arr.push(undefined);
    expect(arr[2]).toBeNaN();
    expect(arr[3]).toBeNull();
    expect(arr[4]).toBeUndefined();
  });
  test('should increase length, when adds elements', () => {
    const arr = new MyArray(1);
    arr.push(3);
    expect(arr).toHaveLength(2);
  });
  test('empty push do nothing', () => {
    const arr = new MyArray(1);
    arr.push();
    expect(arr).toHaveLength(1);
  });
  test('should return length', () => {
    const arr = new MyArray(1, 4, 0);
    const b = arr.length;
    arr.push('hello');
    expect(arr).toHaveLength(4);
    expect(b).not.toBe(arr.length);
  });
  test('push should append all provided values from arguments to the end of current array and increase length', () => {
    const arr = new MyArray(1, 2);
    const b = arr.length;
    arr.push(3, 4);
    expect(arr[2]).toEqual(3);
    expect(arr[3]).toEqual(4);
    expect(b).not.toBe(arr.length);
  });
  test('instance has not Own Property push', () => {
    const arr = new MyArray(1, 2);
    expect(Object.prototype.hasOwnProperty.call(arr, 'push')).toBeFalsy();
  });
  test('result array should be instance of array class', () => {
    const arr = new MyArray(1, 2);
    expect(arr.push).toBeInstanceOf(Function);
  });
});
