import MyArray from '../index';

describe('tests for from method', () => {
  test('class MyArray has method shift', () => {
    expect(MyArray.from).toBeInstanceOf(Function);
  });

  test('should not be a method of the instance', () => {
    const arr = new MyArray();
    expect(Object.prototype.hasOwnProperty.call(arr, 'from')).toBeFalsy();
  });

  test('should be a static method of Class', () => {
    expect(Object.prototype.hasOwnProperty.call(MyArray, 'from')).toBeTruthy();
  });

  test('should have one required argument', () => {
    expect(MyArray.from).toHaveLength(1);
  });

  test('should ignore non-index property in the obj', () => {
    const obj = {
      0: 'first rider',
      1: 'second rider',
      2: 'third rider',
      3: 'fourth rider',
      stadium: 'Motoarena',
      length: 5
    };

    const result = new MyArray(
      'first rider',
      'second rider',
      'third rider',
      'fourth rider',
      undefined
    );

    const test = MyArray.from(obj);
    expect(test).toEqual(result);
  });

  test('instance of MyArray should have the same length as arrayLike', () => {
    const arrayLike = 'hippopotamus';

    expect(MyArray.from(arrayLike)).toHaveLength(arrayLike.length);
  });

  test('should return new instance from array-like thing', () => {
    const arr = new MyArray('f', 'o', 'o');
    expect(MyArray.from('foo')).toStrictEqual(arr);
  });

  test('should create an empty array if thing is not array-like', () => {
    const test = new MyArray();
    expect(MyArray.from(5)).toStrictEqual(test);
    expect(MyArray.from(5)).toHaveLength(0);
  });

  test('result array should be instance of array class', () => {
    expect(MyArray.from('elephant')).toBeInstanceOf(MyArray);
  });

  test('can push custom this to mapFn', () => {
    const arr = new MyArray(1, 2, 3);
    const mockFn = jest.fn();
    const optionalThis = { a: 1 };
    arr.filter(mockFn.mockReturnThis(), optionalThis);
    expect(mockFn).toHaveReturnedWith(optionalThis);
  });
});
