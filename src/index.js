class MyArray {
  constructor() {
    Object.defineProperty(this, '_length', {
      writable: true
    });
    this.createBasicObj(arguments);
  }

  createBasicObj(args) {
    if (args.length === 0) {
      this.length = 0;
    }
    if (args.length === 1 && typeof value === 'number') {
      this.throwErrInvalidLength(args[0]);
      this.length = args[0];
    } else {
      for (let i = 0; i < args.length; i++) {
        this[i] = args[i];
      }
      this.length = args.length;
    }
  }

  [Symbol.iterator]() {
    let index = 0;
    return {
      next: () => ({
        done: index >= this.length,
        value: this[index++]
      })
    };
  }

  set length(value) {
    this.throwErrInvalidLength(value);
    if (this._length > value) {
      for (let i = value; i < this.length; i++) {
        delete this[i];
      }
    }
    this._length = value;
  }

  get length() {
    return this._length;
  }

  push() {
    if (arguments.length !== 0) {
      for (let i = 0; i < arguments.length; i++) {
        this[this.length + i] = arguments[i];
      }
      this.length += arguments.length;
    }
    return this.length;
  }

  pop() {
    let lastEl = this[this.length - 1];
    delete this[this.length - 1];
    this.length--;
    return lastEl;
  }

  shift() {
    let firstEl = this[0];
    for (let i = 0; i < this.length - 1; i++) {
      this[i] = this[i + 1];
    }
    delete this[this.length - 1];
    this.length--;
    return firstEl;
  }

  unshift() {
    if (arguments.length !== 0) {
      let newObj = Object.assign({}, this);
      this.length += arguments.length;
      for (let i = 0; i < this.length; i++) {
        if (i < arguments.length) {
          this[i] = arguments[i];
        } else {
          i - arguments.length in newObj
            ? (this[i] = newObj[i - arguments.length])
            : void 0;
        }
      }
    }
    return this.length;
  }

  static from(arrayLike, myFunc) {
    this.throwErrInvalidFunc(myFunc);
    const newObj = new MyArray();
    if (typeof arrayLike == 'number' && arguments.length == 1) return newObj;
    let iterable = Object(arrayLike);
    let newThisArg = arguments.length > 2 ? arguments[2] : void 0;
    for (let el of iterable) {
      if (arguments.length > 1) {
        el = arguments[1].call(newThisArg, el, newObj.length, iterable);
      }
      newObj[newObj.length++] = el;
    }
    return newObj;
  }

  map(func) {
    let newObj = new MyArray();
    this.throwErrInvalidFunc(func);
    let newThisArg = arguments.length > 1 ? arguments[1] : void 0;
    for (let i = 0; i < this.length; i++) {
      if (i in this) {
        newObj[i] = func.call(newThisArg, this[i], i, this);
        newObj.length++;
      }
    }
    return newObj;
  }

  forEach(func) {
    this.throwErrInvalidFunc(func);
    let newThisArg = arguments.length > 1 ? arguments[1] : void 0;
    for (let i = 0; i < this.length; i++) {
      if (i in this) {
        func.call(newThisArg, this[i], i, this);
      }
    }
  }

  reduce(func) {
    this.throwErrInvalidFunc(func);
    let acc;
    if (this == null) {
      throw new TypeError('Array.prototype.reduce called on null or undefined');
    }

    let k = 0;
    while (k < this.length && !(k in this)) {
      k++;
    }
    if (k >= this.length) {
      throw new TypeError('Reduce of empty array with no initial value');
    }
    if (arguments.length > 1) {
      acc = arguments[1];
    } else {
      acc = this[k];
      k++;
    }
    for (; k < this.length; k++) {
      if (k in this) {
        acc = func(acc, this[k], k, this);
      }
    }

    return acc;
  }

  filter(func) {
    this.throwErrInvalidFunc(func);
    let newObj = new MyArray();
    let newThisArg = arguments.length > 1 ? arguments[1] : void 0;
    for (let i = 0; i < this.length; i++) {
      if (func.call(newThisArg, this[i], i, this)) {
        newObj.push(this[i]);
      }
    }
    return newObj;
  }

  sort() {
    let func;

    if (arguments.length > 0) {
      this.throwErrInvalidFunc(arguments[0]);
      func = arguments[0];
    } else {
      func = function(a, b) {
        a += '';
        b += '';
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }
        return 0;
      };
    }

    for (let i = 0, endI = this.length - 1; i < endI; i++) {
      let wasSwap = false;
      for (let j = 0, endJ = endI - i; j < endJ; j++) {
        if (func(this[j], this[j + 1]) > 0) {
          let temp = this[j + 1];
          this[j + 1] = this[j];
          this[j] = temp;
          wasSwap = true;
        }
      }
      if (!wasSwap) break;
    }

    // empty elements don't convert to undefined

    // let fullEl = 0;
    // for (let i = 0; i < this.length; i++) {
    //   if (i in this) {
    //     fullEl++;
    //   }
    // }

    // for (let i = 0, endI = this.length - 1; i < endI; i++) {
    //   let wasSwap = false;
    //   for (let j = 0, endJ = endI - i; j < endJ; j++) {
    //     if (func(this[j], this[j + 1]) > 0) {
    //       let temp = this[j + 1];
    //       this[j + 1] = this[j];
    //       this[j] = temp;
    //       wasSwap = true;
    //     }
    //   }
    //   if (!wasSwap) break;
    // }
    // if (fullEl < this.length) {
    //   for (let i = fullEl; i < this.length; i++) {
    //     delete this[i];
    //   }
    // }
    return this;
  }

  toString() {
    let str = '';
    for (let i = 0; i < this.length; i++) {
      i in this ? (str += this[i] + ',') : (str += ',');
    }
    return str.slice(0, str.length - 1);
  }

  throwErrInvalidLength(value) {
    if (typeof value !== 'number' || !isFinite(value) || value < 0) {
      throw new RangeError('Invalid MyArr length');
    }
  }

  throwErrInvalidFunc(func) {
    if (typeof func != 'function') {
      throw new TypeError(`${func} is not a function`);
    }
  }
}

export default MyArray;
